# 北京航空航天大学简历模板 base超级简历

> 本模板仅为个人兴趣制作，非官方模板

本项目为北京航空航天大学定制简历模板的doc版本，基于超级简历的标准风格制作。

### 下载地址

[https://gitee.com/warindy/BUAAresume](https://gitee.com/warindy/BUAAresume)

### 样式预览

![北航求职简历模板](https://c1.im5i.com/2021/11/17/JxeL8.jpg)

### 模板使用

- 直接更改word文件即可。

- 使用前**务必**安装字体文件。



By Warindy.